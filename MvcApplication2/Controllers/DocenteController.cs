﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication2.Models;

namespace MvcApplication2.Controllers
{
    public class DocenteController : Controller
    {
        private UsersContext2 db = new UsersContext2();

        //
        // GET: /Docente/

        public ActionResult Index()
        {
            var docentes = db.Docentes.Include(d => d.DepartamentoSalud).Include(d => d.HojaVida).Include(d => d.Rotacion);
            return View(docentes.ToList());
        }
        public ActionResult BuscarEnVacuna(Docente docente)
        {
            var docentes = from b in db.Docentes
                           select b;

            foreach (var b in docentes)
            {
                if (b.num_documento.Equals(docente.num_documento))
                {
                    docente = b;
                }

            }
            if (docente.docenteId == 0)
            {
                return View(docente);
            }
            else
            {
                return RedirectToAction("../Estudiante/PersonalesDS/" + docente.num_documento);
            }
        }
        //
        // GET: /Docente/Details/5

        public ActionResult Details(int id = 0)
        {
            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }

        //
        // GET: /Docente/Create

        public ActionResult Create()
        {
            ViewBag.DepartamentoSaludId = new SelectList(db.DepartamentoSaluds, "DepartamentoSaludId", "nombre");
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre");
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "rotacionId");
            return View();
        }

        //
        // POST: /Docente/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Docente docente)
        {
            if (ModelState.IsValid)
            {
                db.Docentes.Add(docente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartamentoSaludId = new SelectList(db.DepartamentoSaluds, "DepartamentoSaludId", "nombre", docente.DepartamentoSaludId);
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", docente.hojaVidaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", docente.rotacionId);
            return View(docente);
        }

        //
        // GET: /Docente/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartamentoSaludId = new SelectList(db.DepartamentoSaluds, "DepartamentoSaludId", "nombre", docente.DepartamentoSaludId);
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", docente.hojaVidaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", docente.rotacionId);
            return View(docente);
        }

        //
        // POST: /Docente/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Docente docente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(docente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartamentoSaludId = new SelectList(db.DepartamentoSaluds, "DepartamentoSaludId", "nombre", docente.DepartamentoSaludId);
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", docente.hojaVidaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", docente.rotacionId);
            return View(docente);
        }

        //
        // GET: /Docente/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }

        //
        // POST: /Docente/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Docente docente = db.Docentes.Find(id);
            db.Docentes.Remove(docente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        //
        //--------------------- METODOS PARA GESTIONAR LAS VISTAS DE LA-------------------
        //---------------------------- HOJA DE VIDA DE DOCENTES---------------------


        //
        //--------------------- Vista para Logeo del DOCENTE
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Docente docente)
        {
            var docentes = from b in db.Docentes
                           select b;

            foreach (var b in docentes)
            {
                if (b.clave.Equals(docente.clave) && b.num_documento.Equals(docente.num_documento))
                {
                    docente = b;
                }

            }
            if (docente.docenteId == 0)
            {
                return View(docente);
            }
            else
            {
                return RedirectToAction("../Docente/InformacionDocente/" + docente.docenteId);
            }
        }

        public ActionResult Login()
        {

            return View();

        }


        //
        //------------------------- Vista para datos personales del DOCENTE

        public ActionResult InformacionDocente(int id = 0)
        {
            //TempData["notice"] = null;

            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }


        public ActionResult Buscar(Docente docente)
        {
            var docentes = from b in db.Docentes
                              select b;

            foreach (var b in docentes)
            {
                if (b.num_documento.Equals(docente.num_documento))
                {
                    docente = b;
                }

            }
            if (docente.docenteId == 0)
            {
                return View(docente);
            }
            else
            {
                return RedirectToAction("../Docente/InformacionDocenteVista/" + docente.docenteId);
            }
        }
        public ActionResult InformacionDocenteVista(int id = 0)
        {
            //TempData["notice"] = null;

            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InformacionDocente(Docente docente)
        {
            if (ModelState.IsValid)
            {

                HojaVida oHojaVida = db.HojaVidas.Find(docente.hojaVidaId);

                oHojaVida.direccion_manizales = docente.HojaVida.direccion_manizales;
                oHojaVida.correo = docente.HojaVida.correo;
                oHojaVida.estado_civil = docente.HojaVida.estado_civil;
                oHojaVida.num_celular = docente.HojaVida.num_celular;
                oHojaVida.num_telefono = docente.HojaVida.num_telefono;

                oHojaVida.ARL = docente.HojaVida.ARL;
                oHojaVida.Familia = docente.HojaVida.Familia;

                docente.HojaVida = null;
               
                db.Entry(docente).State = EntityState.Modified;
                int numFiles = Request.Files.Count;

                if (Request != null)
                {
                   
                   
                    int uploadedCount = 0;
                    string[] documentos={"doc_identidad","acta_grado","dip_prof","dip_espe","tp_terr","tp_dept","tp_nal","otro"};
                    for (int i = 0; i < numFiles; i++)
                    {
                         HttpPostedFileBase  file = Request.Files[i];
                        if (file.ContentLength > 0)
                        {
                            string fileName = file.FileName;
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];
                            file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                            string path1 = string.Format("{0}/{1}{2}", Server.MapPath("~/Uploads/"), documentos[i] + docente.num_documento, ".jpg");
                            if (System.IO.File.Exists(path1))
                                System.IO.File.Delete(path1);

                            file.SaveAs(path1);
                            uploadedCount++;
                        }
                    }
                }
               
                db.SaveChanges();
                return View(docente);
            }
            return View(docente);
        }




        public ActionResult RotacionDocente(string searchString, int id = 0)
        {

            string rotacion = Request.Params["rotacion"];
            if (rotacion == null)
            {
                ViewBag.id = id;
            }
            else
            {
                Docente docente = db.Docentes.Find(id);
                docente.rotacionId = Convert.ToInt32(rotacion);
                Rotacion rotacionE = db.Rotacions.Find(Convert.ToInt32(rotacion));
                docente.Rotacion = rotacionE;
                db.Entry(rotacionE).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(docente).State = EntityState.Modified;
                db.SaveChanges();

            }
            var docentes = from s in db.Docentes
                              select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                docentes = docentes.Where(s => s.num_documento.Equals(searchString));
            }


            return View(docentes.ToList());
        }




        public ActionResult RotacionDocente2(string searchString, int id = 0)
        {

            string rotacion = Request.Params["rotacion"];
            if (rotacion == null)
            {
                ViewBag.id = id;
            }
            else
            {
                Docente docente = db.Docentes.Find(id);
                docente.rotacionId = Convert.ToInt32(rotacion);
                Rotacion rotacionE = db.Rotacions.Find(Convert.ToInt32(rotacion));
                
                docente.Rotacion = rotacionE;
                db.Entry(rotacionE).State = EntityState.Modified;
                db.SaveChanges();
                docente.Rotacion = rotacionE;
                db.Entry(docente).State = EntityState.Modified;
                db.SaveChanges();

            }


            return RedirectToAction("RotacionDocente/" + rotacion);
        }
    }
}