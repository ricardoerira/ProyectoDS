﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication2.Models;

namespace MvcApplication2.Controllers
{
    public class Default1Controller : Controller
    {
        private pruebaEntities db = new pruebaEntities();

        //
        // GET: /Default1/

        public ActionResult Index()
        {
            return View(db.Departamento .ToList());
        }

        //
        // GET: /Default1/Details/5

        public ActionResult Details(int id = 0)
        {
            Departamento  departamento  = db.Departamento .Find(id);
            if (departamento  == null)
            {
                return HttpNotFound();
            }
            return View(departamento );
        }

        //
        // GET: /Default1/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Default1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Departamento  departamento )
        {
            if (ModelState.IsValid)
            {
                db.Departamento .Add(departamento );
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(departamento );
        }

        //
        // GET: /Default1/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Departamento  departamento  = db.Departamento .Find(id);
            if (departamento  == null)
            {
                return HttpNotFound();
            }
            return View(departamento );
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Departamento  departamento )
        {
            if (ModelState.IsValid)
            {
                db.Entry(departamento ).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(departamento );
        }

        //
        // GET: /Default1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Departamento  departamento  = db.Departamento .Find(id);
            if (departamento  == null)
            {
                return HttpNotFound();
            }
            return View(departamento );
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Departamento  departamento  = db.Departamento .Find(id);
            db.Departamento .Remove(departamento );
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}