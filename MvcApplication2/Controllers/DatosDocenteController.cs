﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication2.Models;

namespace MvcApplication2.Controllers
{
    public class DatosDocenteController : Controller
    {
        private UsersContext2 db = new UsersContext2();

        //
        // GET: /DatosDocente/

        public ActionResult Index()
        {
            var docentes = db.Docentes.Include(d => d.HojaVida).Include(d => d.Rotacion);
            return View(docentes.ToList());
        }

        //
        // GET: /DatosDocente/Details/5

        public ActionResult Details(int id = 0)
        {
            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }

        public ActionResult datosDocente(int id = 0)
        {

            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);

        }

        //
        // GET: /DatosDocente/Create

        public ActionResult Create()
        {
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre");
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo");
            return View();
        }

        //
        // POST: /DatosDocente/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Docente docente)
        {
            if (ModelState.IsValid)
            {
                db.Docentes.Add(docente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", docente.hojaVidaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", docente.rotacionId);
            return View(docente);
        }



        //
        // GET: /DatosDocente/Edit/5 VISTA InformacionDocente

        public ActionResult InformacionDocente(int id = 0)
        {

            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }
        //
        // GET: /DatosDocente/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", docente.hojaVidaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", docente.rotacionId);
            return View(docente);
        }

        //
        // POST: /DatosDocente/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Docente docente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(docente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", docente.hojaVidaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", docente.rotacionId);
            return View(docente);
        }

        //
        // GET: /DatosDocente/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }

        //
        // POST: /DatosDocente/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Docente docente = db.Docentes.Find(id);
            db.Docentes.Remove(docente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}