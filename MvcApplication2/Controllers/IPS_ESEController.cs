﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication2.Models;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace MvcApplication2.Controllers
{
    public class IPS_ESEController : Controller
    {
        private UsersContext2 db = new UsersContext2();

        //
        // GET: /IPS_ESE/

        public ActionResult Index()
        {
            
            var ips_ese = db.IPS_ESE.Include(i => i.Municipio);
            return View(ips_ese.ToList());
        }
        public ActionResult SeleccionRotacionCarta()
        {

            var municipios = db.IPS_ESE.Include(h => h.Municipio);
            List<IPS_ESE> lista = municipios.ToList();
            ViewBag.IPS_ESEId = new SelectList(lista, "IPS_ESEId", "nombre");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SeleccionRotacionCarta(string s)
        {

            IPS_ESE ips = db.IPS_ESE.Find(ViewBag.IPS_ESEId);
            string fecha = ViewBag.fecha;

            


            ReportDocument rptH = new ReportDocument();
            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/reporte.rpt");
            rptH.Load(strRptPath);


            rptH.Database.Tables[0].SetDataSource(db.Estudiantes.ToList());
            rptH.Database.Tables[1].SetDataSource(db.HojaVidas.ToList());
            rptH.Database.Tables[2].SetDataSource(db.Rotacions.ToList());




            rptH.SetParameterValue("presentacion", "A continuación le relaciono las rotaciones de los estudiantes que realizaran su rotación en su institución y los profesores con su horario.");
            rptH.SetParameterValue("fecha", "");






            Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

            return File(stream, "application/pdf");
        }

        public ActionResult SeleccionRotacionContraPrestacion()
        {

            var municipios = db.IPS_ESE.Include(h => h.Municipio);
            List<IPS_ESE> lista = municipios.ToList();
            ViewBag.IPS_ESEId = new SelectList(lista, "IPS_ESEId", "nombre");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SeleccionRotacionContraPrestacion(IPS_ESE ipss)
        {

            IPS_ESE ips = db.IPS_ESE.Find(ipss.IPS_ESEId);
            string fecha = ViewBag.fecha;


            List<Curso> cursos = new List<Curso>();
            cursos = db.Cursoes.ToList();
            ReportDocument rptH = new ReportDocument();
            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/ReporteContraPrestacion.rpt");
            rptH.Load(strRptPath);

           
     

            rptH.Database.Tables[0].SetDataSource(db.Cursoes.Where(d => d.IPS_ESEId == ips.IPS_ESEId).ToList());

            rptH.Database.Tables[1].SetDataSource(db.Induccions.Where(d => d.IPS_ESEId == ips.IPS_ESEId).ToList());


            rptH.SetParameterValue("ips", ips.nombre);
            rptH.SetParameterValue("fecha", "");
            int total = 0;
            if (db.Cursoes.Where(d => d.IPS_ESEId == ips.IPS_ESEId).ToList().Count > 0)
            {   
                 total = db.Cursoes.Where(d => d.IPS_ESEId == ips.IPS_ESEId).Sum(d => d.totalContraprestacion);

            }
            if (db.Induccions.Where(d => d.IPS_ESEId == ips.IPS_ESEId).ToList().Count > 0)
            {
                total += db.Induccions.Where(d => d.IPS_ESEId == ips.IPS_ESEId).Sum(d => d.valor);

            }
            
            rptH.SetParameterValue("total", total);





            Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);




            return File(stream, "application/pdf");
          
        }

        public ActionResult SeleccionRotacionContraPrestacionE()
        {

            var municipios = db.IPS_ESE.Include(h => h.Municipio);
            List<IPS_ESE> lista = municipios.ToList();
            ViewBag.IPS_ESEId = new SelectList(lista, "IPS_ESEId", "nombre");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SeleccionRotacionContraPrestacionE(IPS_ESE ipss)
        {

            IPS_ESE ips = db.IPS_ESE.Find(ipss.IPS_ESEId);
            string fecha = ViewBag.fecha;


            List<Curso> cursos = new List<Curso>();
            cursos = db.Cursoes.ToList();
            ReportDocument rptH = new ReportDocument();
            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/ReporteEquipos.rpt");
            rptH.Load(strRptPath);




            rptH.Database.Tables[0].SetDataSource(db.Equipoes.ToList());

          


            rptH.SetParameterValue("ips", ips.nombre);
            rptH.SetParameterValue("fecha", "");






            Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);




            return File(stream, "application/pdf");

        }
        public ActionResult GeneraReporte(int id = 0)
        {

            IPS_ESE ips = db.IPS_ESE.Find(id);

          
            
            ReportDocument rptH = new ReportDocument();
            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/reporte.rpt");
            rptH.Load(strRptPath);


            rptH.Database.Tables[0].SetDataSource(db.Estudiantes.ToList());
            rptH.Database.Tables[1].SetDataSource(db.HojaVidas.ToList());
            rptH.Database.Tables[2].SetDataSource(db.Rotacions.ToList());

            

           
            rptH.SetParameterValue("presentacion", "A continuación le relaciono las rotaciones de los estudiantes que realizaran su rotación en su institución y los profesores con su horario.");
            rptH.SetParameterValue("fecha", "");






            Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

            return File(stream, "application/pdf");

        }
     
        //
        // GET: /IPS_ESE/Details/5

        public ActionResult Details(int id = 0)
        {
            IPS_ESE ips_ese = db.IPS_ESE.Find(id);
            if (ips_ese == null)
            {
                return HttpNotFound();
            }
            return View(ips_ese);
        }

        //
        // GET: /IPS_ESE/Create

        public ActionResult Create()
        {
            var municipios = db.Municipios.Include(h => h.Departamento);
            List<Municipio> lista = municipios.ToList();

            ViewBag.municipioId = new SelectList(lista, "municipioId", "nombre");
            
            return View();
        }

        //
        // POST: /IPS_ESE/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IPS_ESE ips_ese)
        {
            if (ModelState.IsValid)
            {
                db.IPS_ESE.Add(ips_ese);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.municipioId = new SelectList(db.Municipios, "municipioId", "nombre", ips_ese.municipioId);
            return View(ips_ese);
        }

        //
        // GET: /IPS_ESE/Edit/5

        public ActionResult Edit(int id = 0)
        {
            IPS_ESE ips_ese = db.IPS_ESE.Find(id);
            if (ips_ese == null)
            {
                return HttpNotFound();
            }
            ViewBag.municipioId = new SelectList(db.Municipios, "municipioId", "nombre", ips_ese.municipioId);
            return View(ips_ese);
        }

        //
        // POST: /IPS_ESE/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(IPS_ESE ips_ese)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ips_ese).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.municipioId = new SelectList(db.Municipios, "municipioId", "nombre", ips_ese.municipioId);
            return View(ips_ese);
        }

        //
        // GET: /IPS_ESE/Delete/5

        public ActionResult Delete(int id = 0)
        {
            IPS_ESE ips_ese = db.IPS_ESE.Find(id);
            if (ips_ese == null)
            {
                return HttpNotFound();
            }
            return View(ips_ese);
        }

        //
        // POST: /IPS_ESE/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IPS_ESE ips_ese = db.IPS_ESE.Find(id);
            db.IPS_ESE.Remove(ips_ese);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}