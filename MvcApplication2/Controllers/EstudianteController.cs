﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication2.Models;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace MvcApplication2.Controllers
{
    public class EstudianteController : Controller
    {
        private UsersContext2 db = new UsersContext2();


        public ActionResult BuscarEnVacuna(Estudiante estudiante)
        {
            var estudiantes = from b in db.Estudiantes
                              select b;

            foreach (var b in estudiantes)
            {
                if (b.num_documento.Equals(estudiante.num_documento) || b.codigo.Equals(estudiante.codigo))
                {
                    estudiante = b;
                }

            }
            if (estudiante.estudianteId == 0)
            {
                return View(estudiante);
            }
            else
            {
                return RedirectToAction("../Vacuna/EsquemaVacunacion/" + estudiante.estudianteId);
            }
        }


        //
        // GET: /Estudiante/
        public ActionResult Buscar(Estudiante estudiante)
        {
            var estudiantes = from b in db.Estudiantes
                              select b;

            foreach (var b in estudiantes)
            {
                if (b.num_documento.Equals(estudiante.num_documento) || b.codigo.Equals(estudiante.codigo))
                {
                    estudiante = b;
                }

            }
            if (estudiante.estudianteId == 0)
            {
                return View(estudiante);
            }
            else
            {
                return RedirectToAction("../Estudiante/PersonalesDS/" + estudiante.estudianteId);
            }
        }
        public ActionResult Index()
        {
            var estudiantes = db.Estudiantes.Include(e => e.HojaVida).Include(e => e.Programa).Include(e => e.Rotacion);
            return View(estudiantes.ToList());
        }
        public ActionResult RotacionEstudiante(string searchString, int id = 0)
        {

            string rotacion = Request.Params["rotacion"];
            if (rotacion == null)
            {
                ViewBag.id = id;
            }
            else
            {
                Estudiante estudiante = db.Estudiantes.Find(id);
                estudiante.rotacionId = Convert.ToInt32(rotacion);
                Rotacion rotacionE = db.Rotacions.Find(Convert.ToInt32(rotacion));
                rotacionE.numero_estudiantes = rotacionE.numero_estudiantes + 1;
                estudiante.Rotacion = rotacionE;
                db.Entry(rotacionE).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(estudiante).State = EntityState.Modified;
                db.SaveChanges();

            }
            var estudiantes = from s in db.Estudiantes
                              select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                int ss = Convert.ToInt32(searchString);
                estudiantes = estudiantes.Where(s => s.codigo == ss);
            }


            return View(estudiantes.ToList());
        }
       
        public ActionResult ReporteEstudiante(string searchString, int id = 0)
        {

           
              

            
            var estudiantes = from s in db.Estudiantes
                              select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                int ss = Convert.ToInt32(searchString);
                estudiantes = estudiantes.Where(s => s.codigo == ss);
            }


            return View(estudiantes.ToList());
        }
        public ActionResult ReporteEstudianteA(int id = 0)
        {




            Estudiante estudiante = db.Estudiantes.Find(id);

            ReportDocument rptH = new ReportDocument();
            string strRptPath = System.Web.HttpContext.Current.Server.MapPath("~/reporteEstudiante.rpt");
            rptH.Load(strRptPath);


            var vacunas = from b in db.Vacunas
                              select b;
            List<Vacuna> listav = new List<Vacuna>();
       int i=0;

       foreach (var b in vacunas)
            {
                  if (b.fecha_vacunacion > DateTime.Now.Date.AddMonths(-20) && b.hojaVidaId==estudiante.hojaVidaId)
                                         
                  
                  {


                      listav.Add(b);
              
                  }

                i++;
            }
            rptH.Database.Tables[0].SetDataSource(listav.ToList());
     
            rptH.SetParameterValue("programa", estudiante.Programa.nombre);
            rptH.SetParameterValue("modalidad", estudiante.modalidad + "");
            rptH.SetParameterValue("semestre", estudiante.semestre + "");
            rptH.SetParameterValue("estadom", estudiante.estado_academico + "");



            rptH.SetParameterValue("tipodoc", estudiante.tipo_documento);
            rptH.SetParameterValue("numdoc", estudiante.num_documento);
            rptH.SetParameterValue("codigo", estudiante.codigo);
            rptH.SetParameterValue("genero", estudiante.HojaVida.genero + "");
            rptH.SetParameterValue("nombre", estudiante.HojaVida.primer_nombre+" "+estudiante.HojaVida.segundo_nombre);
            rptH.SetParameterValue("apellidos", estudiante.HojaVida.primer_apellido + " " + estudiante.HojaVida.segundo_apellido);
            rptH.SetParameterValue("fecha_nacimiento", estudiante.HojaVida.fecha_nacimiento + "");
            rptH.SetParameterValue("hemoclasificacion", estudiante.HojaVida.hemoclasificacion + "");
            rptH.SetParameterValue("dpto_procedencia", estudiante.HojaVida.departamento_procedencia + "");
            rptH.SetParameterValue("mun_procedencia", estudiante.HojaVida.municipio_procedencia + "");
            rptH.SetParameterValue("barrio_procedencia", estudiante.barrio_procedencia + "");
            rptH.SetParameterValue("dir_procedencia", estudiante.direccion_procedencia + "");
            rptH.SetParameterValue("dir_local", estudiante.HojaVida.direccion_manizales + "");
            rptH.SetParameterValue("image", estudiante.HojaVida.imagen_DI + "");


            rptH.SetParameterValue("tel_proc", estudiante.telefono_procedencia+"");
            rptH.SetParameterValue("tel_local", "");
            rptH.SetParameterValue("estado_civil", estudiante.HojaVida.estado_civil+"");
            rptH.SetParameterValue("num_hijos", estudiante.HojaVida.hijos + "");
            rptH.SetParameterValue("mail", estudiante.HojaVida.correo + "");

            rptH.SetParameterValue("num_cel", estudiante.HojaVida.num_celular + "");

            
            rptH.SetParameterValue("nombre_padre", estudiante.HojaVida.Familia.primer_nombre_padre + " " + estudiante.HojaVida.Familia.primer_apellido_padre + " " + estudiante.HojaVida.Familia.segundo_apellido_padre);
            rptH.SetParameterValue("direccion_padre", estudiante.HojaVida.Familia.direccion_padre + "");
            rptH.SetParameterValue("nombre_madre", estudiante.HojaVida.Familia.primer_nombre_madre + " " + estudiante.HojaVida.Familia.primer_apellido_madre + " " + estudiante.HojaVida.Familia.segundo_apellido_madre);
            rptH.SetParameterValue("direccion_madre", estudiante.HojaVida.Familia.direccion_madre + "");
            rptH.SetParameterValue("nombre_acudiente", estudiante.HojaVida.Familia.primer_nombre_acudiente + " " + estudiante.HojaVida.Familia.primer_apellido_acudiente + " " + estudiante.HojaVida.Familia.segundo_apellido_acudiente);
            rptH.SetParameterValue("direccion_acudiente", estudiante.HojaVida.Familia.direccion_acudiente + "");
          



            Stream stream = rptH.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);

            return File(stream, "application/pdf");

        }
        public ActionResult RotacionEstudiante2(string searchString, int id = 0)
        {

            string rotacion = Request.Params["rotacion"];
            if (rotacion == null)
            {
                ViewBag.id = id;
            }
            else
            {
                Estudiante estudiante = db.Estudiantes.Find(id);
                estudiante.rotacionId = Convert.ToInt32(rotacion);
                Rotacion rotacionE = db.Rotacions.Find(Convert.ToInt32(rotacion));
                rotacionE.numero_estudiantes = rotacionE.numero_estudiantes + 1;
                estudiante.Rotacion = rotacionE;
                db.Entry(rotacionE).State = EntityState.Modified;
                db.SaveChanges();
                estudiante.Rotacion = rotacionE;
                db.Entry(estudiante).State = EntityState.Modified;
                db.SaveChanges();

            }


            return RedirectToAction("RotacionEstudiante/" + rotacion);
        }
       
      
        public ActionResult Details(int id = 0)
        {
            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);
        }



        //
        // GET: /Estudiante/Create

        public ActionResult Create()
        {
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre");
            ViewBag.programaId = new SelectList(db.Programas, "programaId", "nombre");
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "rotacionId");
            return View();
        }

        //
        // POST: /Estudiante/Create

        [HttpPost]


        [ValidateAntiForgeryToken]
        public ActionResult Create(Estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
                
                db.Estudiantes.Add(estudiante);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", estudiante.hojaVidaId);
            ViewBag.programaId = new SelectList(db.Programas, "programaId", "nombre", estudiante.programaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", estudiante.rotacionId);
            return View(estudiante);
        }

        //
        // GET: /Estudiante/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", estudiante.hojaVidaId);
            ViewBag.programaId = new SelectList(db.Programas, "programaId", "nombre", estudiante.programaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", estudiante.rotacionId);
            return View(estudiante);
        }

        //
        // POST: /Estudiante/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estudiante).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.hojaVidaId = new SelectList(db.HojaVidas, "hojaVidaId", "primer_nombre", estudiante.hojaVidaId);
            ViewBag.programaId = new SelectList(db.Programas, "programaId", "nombre", estudiante.programaId);
            ViewBag.rotacionId = new SelectList(db.Rotacions, "rotacionId", "tipo", estudiante.rotacionId);
            return View(estudiante);
        }

        //
        // GET: /Estudiante/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);
        }

        //
        // POST: /Estudiante/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Estudiante estudiante = db.Estudiantes.Find(id);
            db.Estudiantes.Remove(estudiante);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        //
        //--------------------- METODOS PARA GESTIONAR LAS VISTAS DE LA-------------------
        //---------------------------- HOJA DE VIDA DE ESTUDIANTES-------------------


        //
        //--------------------- Vista para Logeo del estudiante
        
       
        public ActionResult Login()
        {

            return View();

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Estudiante estudiante)
        {
            var estudiantes = from b in db.Estudiantes
                              select b;

            foreach (var b in estudiantes)
            {
                if (b.clave.Equals(estudiante.clave) && b.num_documento.Equals(estudiante.num_documento))
                {
                    estudiante = b;
                }

            }
            if (estudiante.estudianteId == 0)
            {
                return View(estudiante);
            }
            else
            {
                return RedirectToAction("../Estudiante/Personales/" + estudiante.estudianteId);
            }
        }


        //
        //------------------------- Vista para datos personales del estudiante

        public ActionResult Personales(int id = 0)
        {
            TempData["notice"] = null;

            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            
                            
                        
            return View(estudiante);
        }


        


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Personales(Estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
             
              HojaVida oHojaVida=  db.HojaVidas.Find(estudiante.hojaVidaId);

              oHojaVida.direccion_manizales = estudiante.HojaVida.direccion_manizales;
              oHojaVida.hijos = estudiante.HojaVida.hijos;
              oHojaVida.correo = estudiante.HojaVida.correo;
              oHojaVida.estado_civil = estudiante.HojaVida.estado_civil;
              oHojaVida.num_celular = estudiante.HojaVida.num_celular;
              oHojaVida.num_telefono = estudiante.HojaVida.num_telefono;
              oHojaVida.Familia = estudiante.HojaVida.Familia;
              
              estudiante.HojaVida = null;

                db.Entry(estudiante).State = EntityState.Modified;

                if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["InputFile"];

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    string extension = System.IO.Path.GetExtension(Request.Files["InputFile"].FileName);
                    string path1 = string.Format("{0}/{1}{2}", Server.MapPath("~/Uploads/"),"cedula_"+ estudiante.codigo, ".jpg");
                    if (System.IO.File.Exists(path1))
                        System.IO.File.Delete(path1);

                    Request.Files["InputFile"].SaveAs(path1);
                }
            }

                db.SaveChanges();
                return View(estudiante);
            }
            return View(estudiante);
        }
        public String ImagePath(Estudiante estudiante)
        {
            
            {
                return "~/Uploads/img/cedula_"+estudiante.codigo+".jpg";
            }

        }


        //
        //------------------------- Vista para datos familia del estudiante

        public ActionResult Familiares(int id = 0)
        {

            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);

        }

        public ActionResult FamiliaresDS(int id = 0)
        {

            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);

        }

        public ActionResult PersonalesDS(int id = 0)
        {
            TempData["notice"] = null;

            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }



            return View(estudiante);
        }


        public ActionResult Personales1(int id = 0)
        {
            TempData["notice"] = null;

            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }



            return View(estudiante);
        }


        //
        //------------------------- Vista para datos academicos del estudiante

        public ActionResult Academicos(int id = 0)
        {

            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);

        }

        public ActionResult AcademicosDS(int id = 0)
        {

            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);

        }


        //
        //------------------------- Vista para datos de vacunacion del estudiante

        public ActionResult CarnetVacunacion(int id = 0)
        {
           

            Estudiante estudiante = db.Estudiantes.Find(id);
          
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            ViewBag.Details = estudiante.HojaVida.Vacunas;
            return View(estudiante);

        }

        public ActionResult CarnetVacunacionDS(int id = 0)
        {


            Estudiante estudiante = db.Estudiantes.Find(id);

            if (estudiante == null)
            {
                return HttpNotFound();
            }
            ViewBag.Details = estudiante.HojaVida.Vacunas;
            return View(estudiante);

        }


        //
        //------------------------- Vista para datos de salud del estudiante
        public ActionResult Salud(int id = 0)
        {
            string vacuna = Request.Params["vacuna"];
            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);

        }

        public ActionResult SaludDS(int id = 0)
        {
            string vacuna = Request.Params["vacuna"];
            Estudiante estudiante = db.Estudiantes.Find(id);
            if (estudiante == null)
            {
                return HttpNotFound();
            }
            return View(estudiante);

        }


        //
        //------------------------- METODO PARA CARGAR ARCHIVOS
        public ActionResult save()
        {
            int idestudiante = Int32.Parse(Request.Params["idEstudiante"]);
            Estudiante estudiante = db.Estudiantes.Find(idestudiante);

            if (estudiante == null)
            {
                return HttpNotFound();
            }  
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["InputFile"];

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    string extension = System.IO.Path.GetExtension(Request.Files["InputFile"].FileName);
                    string path1 = string.Format("{0}/{1}{2}", Server.MapPath("~/"), "cedula_", extension);
                    if (System.IO.File.Exists(path1))
                        System.IO.File.Delete(path1);

                    Request.Files["InputFile"].SaveAs(path1);
                }
            }
            return RedirectToAction("../Estudiante/Personales/" + estudiante.estudianteId);

            // return View("Personales");
        }
    }

}